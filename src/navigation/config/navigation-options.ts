import type {
  StackCardInterpolationProps,
  StackCardInterpolatedStyle,
} from '@react-navigation/stack';
import { Animated } from 'react-native';
import { Colors } from 'theme';

const { multiply } = Animated;

const horizontalTransition = ({
  current,
  next,
  inverted,
  layouts: { screen },
}: StackCardInterpolationProps): StackCardInterpolatedStyle => {
  const translateFocused = multiply(
    current.progress.interpolate({
      inputRange: [0, 1],
      outputRange: [screen.width + 40, 0],
      extrapolate: 'clamp',
    }),
    inverted,
  );

  const translateUnfocused = next
    ? multiply(
      next.progress.interpolate({
        inputRange: [0, 1],
        outputRange: [0, (screen.width + 40) * -1],
        extrapolate: 'clamp',
      }),
      inverted,
    )
    : 0;

  return {
    cardStyle: {
      backgroundColor: Colors.FULL_SHADE,
      transform: [
        // Translation for the animation of the current card
        { translateX: translateFocused },
        // Translation for the animation of the card on top of this
        { translateX: translateUnfocused },
      ],
    },
  };
};

const defaultStackNavigatorOptions = {
  headerShown: false,
  cardShadowEnabled: false,
  cardOverlayEnabled: false,
  cardStyle: {
    backgroundColor: Colors.FULL_SHADE,
  },
  cardStyleInterpolator: horizontalTransition,
};

export default defaultStackNavigatorOptions;
