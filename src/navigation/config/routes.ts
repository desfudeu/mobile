export enum NavigatorRoutes {
  SPLASH = 'SPLASH',
  MAIN = 'MAIN',
  DRAWER = 'DRAWER',
  BOTTOM_TAB = 'BOTTOM_TAB',
}

export enum BottomTabRoutes {
  HOT = 'HOT',
  TRENDING = 'TRENDING',
  FRESH = 'FRESH',
}

export enum MainRoutes {
  HOME = 'HOME',
}
