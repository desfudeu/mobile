import React from 'react';
import {
  createDrawerNavigator,
} from '@react-navigation/drawer';
import {
  SafeAreaView,
  StyleSheet,
} from 'react-native';

import DrawerComponent from 'components/navigation/drawer.component';

import BottomTabNavigator from 'navigation/navigators/bottom-tab.navigator';

import screenOptions from 'navigation/config/navigation-options';

import {
  NavigatorRoutes,
} from 'navigation/config/routes';

import {
  TopBanner,
} from 'components/admob';
import { Colors } from 'theme';

const styles = StyleSheet.create({
  appContainer: {
    flex: 1,
    backgroundColor: Colors.FULL_SHADE,
  },
});

const Drawer = createDrawerNavigator();

export default (): React.ReactElement => (
  <SafeAreaView
    style={styles.appContainer}
  >
    <TopBanner />
    <Drawer.Navigator
      initialRouteName={NavigatorRoutes.BOTTOM_TAB}
      drawerContent={() => <DrawerComponent />}
      screenOptions={screenOptions}
    >
      <Drawer.Screen name={NavigatorRoutes.BOTTOM_TAB} component={BottomTabNavigator} />
    </Drawer.Navigator>
  </SafeAreaView>
);
