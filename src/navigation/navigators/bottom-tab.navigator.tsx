import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import BottomTabIcon from 'components/navigation/bottom-tab-icon.component';

import {
  BottomTabRoutes,
} from 'navigation/config/routes';

import homeIcon from 'assets/icon/home.svg';
import trendingIcon from 'assets/icon/trending.svg';
import fresh from 'assets/icon/fresh.svg';

import HomeScreen from 'screens/home/home.screen';
import FreshScreen from 'screens/home/fresh.screen';
import { Colors, FontFamilies, Spacing } from 'theme';
import { StyleSheet } from 'react-native';

const Tab = createBottomTabNavigator();

interface Map {
  [key: string]: string
}

const RouteIconMap: Map = {
  [BottomTabRoutes.HOT]: homeIcon,
  [BottomTabRoutes.TRENDING]: trendingIcon,
  [BottomTabRoutes.FRESH]: fresh,
};

const styles = StyleSheet.create({
  barStyle: {
    height: 72,
    paddingBottom: Spacing.XS,
  },
  tabLabel: {
    fontFamily: FontFamilies.PRIMARY_MEDIUM,
  },
});

export default (): React.ReactElement => (
  <Tab.Navigator
    initialRouteName={BottomTabRoutes.HOT}
    screenOptions={({ route }) => ({
      tabBarIcon: ({ focused, size }) => (
        <BottomTabIcon icon={RouteIconMap[route.name]} size={size} focused={focused} />
      ),
    })}
    tabBarOptions={{
      activeTintColor: Colors.EMPTY_SHADE,
      inactiveTintColor: Colors.MEDIUM_SHADE,
      labelStyle: styles.tabLabel,
      style: styles.barStyle,
    }}
    lazy
  >
    <Tab.Screen
      name={BottomTabRoutes.HOT}
      component={HomeScreen}
    />
    <Tab.Screen
      name={BottomTabRoutes.TRENDING}
      component={HomeScreen}
    />
    <Tab.Screen
      name={BottomTabRoutes.FRESH}
      component={FreshScreen}
    />
  </Tab.Navigator>
);
