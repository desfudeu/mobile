import React from 'react';

import { NavigationContainer } from '@react-navigation/native';

import { createStackNavigator } from '@react-navigation/stack';

import stackNavigationOptions from 'navigation/config/navigation-options';

import {
  NavigatorRoutes,
} from 'navigation/config/routes';

import SplashScreen from 'screens/loading/splash.screen';
import DrawerNavigator from 'navigation/navigators/drawer.navigator';
import { NAVIGATOR_THEME } from 'theme';

const MainRouter = createStackNavigator();

export default (): React.ReactElement => (
  <NavigationContainer
    theme={NAVIGATOR_THEME}
  >
    <MainRouter.Navigator screenOptions={stackNavigationOptions}>
      <MainRouter.Screen name={NavigatorRoutes.SPLASH} component={SplashScreen} />
      <MainRouter.Screen name={NavigatorRoutes.MAIN} component={DrawerNavigator} />
    </MainRouter.Navigator>
  </NavigationContainer>
);
