import 'react-native-gesture-handler';
import React from 'react';
import { Provider as ReduxProvider } from 'react-redux';
import TranslationProvider from 'locales/provider';

import Router from 'navigation';
import store from 'store';

const App: React.FC = () => (
  <TranslationProvider>
    <ReduxProvider store={store}>
      <Router />
    </ReduxProvider>
  </TranslationProvider>
);

export default App;
