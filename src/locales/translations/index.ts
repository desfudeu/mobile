import pt from './pt-br.json';
import en from './en-us.json';

const translations = {
  pt,
  en,
};

export default translations;
