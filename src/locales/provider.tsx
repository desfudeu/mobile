import React, { createContext } from 'react';
import i18n from 'services/i18n';

const TranslationContext = createContext({
  i18n,
});

const TranslationProvider: React.FC = ({
  children,
}) => (
  <TranslationContext.Provider
    value={{
      i18n,
    }}
  >
    {children}
  </TranslationContext.Provider>
);

export default TranslationProvider;
