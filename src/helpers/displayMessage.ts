import Snackbar from 'react-native-snackbar';
import { Colors } from 'theme';
import i18n from 'services/i18n';

interface Action {
  text: string;
  textColor: Colors | string;
  onPress: () => void;
}

const defaultAction = {
  text: 'OK',
  textColor: Colors.EMPTY_SHADE,
};

export const displayActionMessage = (
  text: string,
  length?: number,
  action?: Action,
): void => {
  Snackbar.show({
    text: i18n.t(text),
    duration: length || Snackbar.LENGTH_SHORT,
    backgroundColor: Colors.PRIMARY,
    action: {
      ...defaultAction,
      ...action,
    },
  });
};

export const displayErrorMessage = (
  text: string,
  length?: number,
  action?: Action,
): void => {
  Snackbar.show({
    text: i18n.t(text),
    duration: length || Snackbar.LENGTH_SHORT,
    backgroundColor: Colors.NEGATIVE,
    action: {
      ...defaultAction,
      ...action,
    },
  });
};

export const displaySuccessMessage = (
  text: string,
  length?: number,
  action?: Action,
): void => {
  Snackbar.show({
    text: i18n.t(text),
    duration: length || Snackbar.LENGTH_SHORT,
    backgroundColor: Colors.POSITIVE,
    action: {
      ...defaultAction,
      ...action,
    },
  });
};
