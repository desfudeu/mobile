import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import { getLocales } from 'react-native-localize';

import translations from 'locales/translations';

i18n
  .use(initReactI18next)
  .init({
    lng: getLocales()[0].languageCode,
    fallbackLng: 'pt',
    resources: translations,
  });

export default i18n;
