import axios from 'axios';
import Config from 'react-native-config';

const api = axios.create({
  timeout: 15000,
  baseURL: Config.API_URL,
});

export default api;

export enum RoutePrefix {
  USER = '/user',
  MEME = '/meme',
  REACTION = '/reaction',
}
