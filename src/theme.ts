export enum Colors {
  PRIMARY = '#55A630',
  PRIMARY_LIGHT = '#AACC00',
  PRIMARY_DARK = '#007F5F',

  SECONDARY = '#DDDF00',
  SECONDARY_LIGHT = '#FFFF3F',
  SECONDARY_DARK = '#BFD200',

  POSITIVE = '#2B9348',
  NEGATIVE = '#CD3500',
  WARNING = '#EEEF20',
  INFO = '#181AB7',
  SUPPORT = '#3030A5',

  EMPTY_SHADE = '#F9F9F9',
  LIGHT_SHADE = '#EDECE6',
  MEDIUM_SHADE = '#828282',
  DARK_SHADE = '#363636',
  FULL_SHADE = '#121212',
}

export enum Spacing {
  XXXS = 2,
  XXS = 4,
  XS = 8,
  S = 12,
  M = 16,
  L = 20,
  XL = 24,
  XXL = 28,
  XXL2 = 32,
  XXL3 = 36,
  XXL4 = 40,
  XXL5 = 44,
  XXL6 = 48,
  XXL7 = 52,
  XXL8 = 56,
  XXL9 = 60,
  XXL10 = 64,
}

export enum FontSizes {
  H1 = 35,
  H2 = 30,
  H3 = 25,
  H4 = 20,
  H5 = 16,
  H6 = 14,
  H7 = 12,
  H8 = 10,
  H9 = 8,
}

export enum FontFamilies {
  PRIMARY_LIGHT = 'Montserrat-Light',
  PRIMARY_REGULAR = 'Montserrat-Regular',
  PRIMARY_MEDIUM = 'Montserrat-Medium',
  PRIMARY_SEMIBOLD = 'Montserrat-SemiBold',
  PRIMARY_BOLD = 'Montserrat-Bold',
  PRIMARY_EXTRABOLD = 'Montserrat-ExtraBold',
  PRIMARY_BLACK = 'Montserrat-Black',
}

export const NAVIGATOR_THEME = {
  dark: true,
  colors: {
    background: Colors.FULL_SHADE,
    primary: Colors.PRIMARY,
    border: Colors.LIGHT_SHADE,
    card: Colors.FULL_SHADE,
    notification: Colors.INFO,
    text: Colors.EMPTY_SHADE,
  },
};
