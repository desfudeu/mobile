import React, { useCallback } from 'react';
import { useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
import Video from 'react-native-video';
import entrance from 'assets/video/entrance.mp4';
import { NavigatorRoutes } from 'navigation/config/routes';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import { configureAuthStateChangeCallback } from 'store/action/auth.action';
import Config from 'react-native-config';
import { getUserReactions } from 'store/action/reaction.action';

export default (): React.ReactElement => {
  const { reset } = useNavigation();
  const dispatch = useDispatch();

  const onAnimationEnd = useCallback(() => {
    GoogleSignin.configure({
      webClientId: Config.GOOGLE_CLIENT_ID,
    });

    dispatch(configureAuthStateChangeCallback(() => {
      dispatch(getUserReactions());
    }));

    reset({
      routes: [{
        name: NavigatorRoutes.MAIN,
      }],
      index: 0,
    });
  }, []);

  return (
    <Video
      source={entrance}
      resizeMode="cover"
      style={{
        flex: 1,
      }}
      onEnd={onAnimationEnd}
    />
  );
};
