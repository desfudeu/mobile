import React, { useEffect, useState, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ApplicationState } from 'store';
import {
  fetchFreshMemes,
} from 'store/action/meme.action';
import { displayActionMessage } from 'helpers/displayMessage';
import { MemeList } from 'components/meme';

const MEMES_PER_PAGE = 10;

export default (): React.ReactElement => {
  const dispatch = useDispatch();

  const [page, setPage] = useState(0);

  const {
    fresh_memes,
    loading_fresh_memes,
  } = useSelector((state: ApplicationState) => state.memeReducer);

  useEffect(() => {
    const SHOULD_RESET_LIST = page === 0;

    dispatch(fetchFreshMemes(
      'fresh',
      page,
      MEMES_PER_PAGE,
      SHOULD_RESET_LIST,
    ));
  }, [page]);

  const resetMemeList = useCallback(() => {
    setPage(0);
  }, []);

  const loadNextPage = useCallback(() => {
    const NEXT_PAGE = page + 1;

    if (fresh_memes.length === NEXT_PAGE * MEMES_PER_PAGE) {
      setPage(NEXT_PAGE);
    } else {
      displayActionMessage('You have reached bottom');
    }
  }, [page, fresh_memes]);

  return (
    <MemeList
      memes={fresh_memes}
      refreshing={loading_fresh_memes}
      onRefresh={resetMemeList}
      onEndReached={loadNextPage}
    />
  );
};
