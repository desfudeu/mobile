import React, { useEffect } from 'react';
import {
  View,
} from 'react-native';
import api from 'services/api';

export default (): React.ReactElement => {
  const fetchMemes = async () => {
    try {
      const { data } = await api.get('/meme', {
        params: {
          rank: 'fresh',
          page: 0,
          limit: 10,
        },
      });
      // eslint-disable-next-line no-console
      console.log(data);
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log({ ...e });
    }
  };
  useEffect(() => {
    fetchMemes();
  }, []);
  return (
    <View />
  );
};
