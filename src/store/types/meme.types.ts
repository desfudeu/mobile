export enum MemeActionsTypes {
  LOADING_FETCH_FRESH_MEMES = 'LOADING_FETCH_FRESH_MEMES',
  ERROR_FETCH_FRESH_MEMES = 'LOADING_FRESH_MEMES',
  FETCH_FRESH_MEMES = 'FETCH_FRESH_MEMES',

  LOADING_POST_MEME = 'LOADING_POST_MEME',
  ERROR_POST_MEME = 'ERROR_POST_MEME',
  POST_MEME = 'POST_MEME',
}

export interface Meme {
  _id: string;
  title?: string;
  mediaUrl: string;
}

export interface FetchMemePayload {
  memes: Meme[];
  reset: boolean;
}

export interface StoreUserAction {
  type: MemeActionsTypes;
  payload?: FetchMemePayload;
}

export interface MemeState {
  loading_fresh_memes: boolean;
  error_fresh_memes: boolean;
  fresh_memes: Meme[];

  loading_post_meme: boolean;
  error_post_meme: boolean;
}

export interface MemePayloadTypes {
  type: MemeActionsTypes;
  payload?: FetchMemePayload;
}
