export enum ReactionActionTypes {
  LOADING_USER_REACTION = 'LOADING_USER_REACTION',
  ERROR_USER_REACTION = 'ERROR_USER_REACTION',
  USER_REACTION = 'USER_REACTION',
}

export interface Reaction {
  _id: string;
  memeId: string;
  reactionType: ReactionType;
}

export enum ReactionType {
  UPVOTE = 'upvote',
  DOWNVOTE = 'downvote',
}

export type ReactionObjectMap = {
  [key: string]: ReactionType
};

export interface ReactionState {
  user_reactions: ReactionObjectMap;
  loading_user_reactions: boolean;
  error_user_reactions: boolean;
}

export interface ReactionPayloadTypes {
  type: ReactionActionTypes,
  payload?: ReactionObjectMap
}
