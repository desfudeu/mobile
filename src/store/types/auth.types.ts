export enum AuthActionsTypes {
  SIGNIN_ATTEMPT = 'SIGNIN_ATTEMPT',

  SIGNIN = 'SIGNIN',
  SIGNOUT = 'SIGNOUT',

  SEND_RESET_PASSWORD_EMAIL = 'SEND_RESET_PASSWORD_EMAIL',
  SEND_RESET_PASSWORD_EMAIL_FAIL = 'SEND_RESET_PASSWORD_EMAIL_FAIL',
  SEND_RESET_PASSWORD_EMAIL_SUCCESS = 'SEND_RESET_PASSWORD_EMAIL_SUCCESS',

  SIGNIN_WITH_EMAIL_AND_PASSWORD = 'SIGNIN_WITH_EMAIL_AND_PASSWORD',

  SIGNUP_WITH_EMAIL_AND_PASSWORD = 'SIGNUP_WITH_EMAIL_AND_PASSWORD',
}

export interface AuthState {
  loading_auth: boolean;
  user: User;
  auth_failed: boolean;
}

export interface AuthPayloadTypes {
  type: AuthActionsTypes;
  payload?: User;
}

export interface User {
  user_name?: string;
  user_email?: string;
  user_id?: string;
  photo_url?: string;
}

export interface StoreUserAction {
  type: AuthActionsTypes;
  payload?: User;
}
