import { AuthActionsTypes, AuthState, AuthPayloadTypes } from 'store/types/auth.types';

const INITIAL_STATE: AuthState = {
  loading_auth: false,
  user: {},
  auth_failed: false,
};

export default (state = INITIAL_STATE, action: AuthPayloadTypes): AuthState => {
  switch (action.type) {
    case AuthActionsTypes.SIGNIN_ATTEMPT:
      return {
        ...state,
        loading_auth: true,
      };
    case AuthActionsTypes.SIGNIN:
      return {
        ...state,
        user: {
          user_name: action.payload?.user_name,
          user_email: action.payload?.user_email,
          user_id: action.payload?.user_id,
          photo_url: action.payload?.photo_url,
        },
        loading_auth: false,
        auth_failed: false,
      };
    case AuthActionsTypes.SIGNOUT:
      return INITIAL_STATE;

    default:
      return state;
  }
};
