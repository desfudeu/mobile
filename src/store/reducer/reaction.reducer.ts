import { ReactionState, ReactionPayloadTypes, ReactionActionTypes } from 'store/types/reaction.types';

const INITIAL_STATE: ReactionState = {
  user_reactions: {},
  loading_user_reactions: false,
  error_user_reactions: false,
};

export default (state = INITIAL_STATE, action: ReactionPayloadTypes): ReactionState => {
  switch (action.type) {
    case ReactionActionTypes.LOADING_USER_REACTION:
      return {
        ...state,
        loading_user_reactions: true,
        error_user_reactions: false,
      };
    case ReactionActionTypes.ERROR_USER_REACTION:
      return {
        ...state,
        loading_user_reactions: false,
        error_user_reactions: true,
      };
    case ReactionActionTypes.USER_REACTION:
      return {
        ...state,
        loading_user_reactions: false,
        error_user_reactions: false,
        user_reactions: action.payload || {},
      };
    default:
      return state;
  }
};
