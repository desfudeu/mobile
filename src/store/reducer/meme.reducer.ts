import { MemeActionsTypes, MemeState, MemePayloadTypes } from 'store/types/meme.types';

const INITIAL_STATE: MemeState = {
  loading_fresh_memes: false,
  error_fresh_memes: false,
  fresh_memes: [],

  loading_post_meme: false,
  error_post_meme: false,
};

export default (state = INITIAL_STATE, action: MemePayloadTypes): MemeState => {
  switch (action.type) {
    case MemeActionsTypes.LOADING_FETCH_FRESH_MEMES:
      return {
        ...state,
        loading_fresh_memes: true,
        error_fresh_memes: false,
      };
    case MemeActionsTypes.ERROR_FETCH_FRESH_MEMES:
      return {
        ...state,
        loading_fresh_memes: false,
        error_fresh_memes: true,
      };
    case MemeActionsTypes.FETCH_FRESH_MEMES:
      return {
        ...state,
        fresh_memes: action?.payload?.reset
          ? (action?.payload?.memes || [])
          : [...state.fresh_memes, ...(action?.payload?.memes || [])],
        loading_fresh_memes: false,
        error_fresh_memes: false,
      };

    case MemeActionsTypes.LOADING_POST_MEME:
      return {
        ...state,
        loading_post_meme: true,
        error_post_meme: false,
      };
    case MemeActionsTypes.ERROR_POST_MEME:
      return {
        ...state,
        loading_post_meme: false,
        error_post_meme: true,
      };
    case MemeActionsTypes.POST_MEME:
      return {
        ...state,
        loading_post_meme: false,
        error_post_meme: false,
      };

    default:
      return state;
  }
};
