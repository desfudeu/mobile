import {
  combineReducers, applyMiddleware, createStore, Store,
} from 'redux';
import ReduxThunk from 'redux-thunk';

import authReducer from 'store/reducer/auth.reducer';
import memeReducer from 'store/reducer/meme.reducer';
import reactionReducer from 'store/reducer/reaction.reducer';

import { AuthState } from 'store/types/auth.types';
import { MemeState } from 'store/types/meme.types';
import { ReactionState } from './types/reaction.types';

const rootReducer = combineReducers({
  authReducer,
  memeReducer,
  reactionReducer,
});

export interface ApplicationState {
  authReducer: AuthState;
  memeReducer: MemeState;
  reactionReducer: ReactionState;
}

const store: Store<ApplicationState> = createStore(rootReducer, {}, applyMiddleware(ReduxThunk));

export default store;
