import FormData from 'form-data';
import api, { RoutePrefix } from 'services/api';
import { DocumentPickerResponse } from 'react-native-document-picker';
import { Dispatch } from 'redux';
import { MemeActionsTypes, MemePayloadTypes } from 'store/types/meme.types';

export const fetchFreshMemes = (
  rank: string,
  page: number,
  limit: number,
  reset?: boolean,
) => async (dispatch: Dispatch<MemePayloadTypes>):Promise<void> => {
  dispatch({
    type: MemeActionsTypes.LOADING_FETCH_FRESH_MEMES,
  });
  try {
    const { data } = await api.get(RoutePrefix.MEME, {
      params: {
        rank,
        page,
        limit,
      },
    });

    dispatch({
      type: MemeActionsTypes.FETCH_FRESH_MEMES,
      payload: {
        memes: data.memes,
        reset: !!reset,
      },
    });
  } catch (e) {
    dispatch({
      type: MemeActionsTypes.ERROR_FETCH_FRESH_MEMES,
    });
  }
};

export const createPost = (
  title: string,
  media: DocumentPickerResponse,
) => async (dispatch: Dispatch<MemePayloadTypes>):Promise<void> => {
  dispatch({
    type: MemeActionsTypes.LOADING_POST_MEME,
  });
  try {
    const body = new FormData();

    body.append('title', title);
    body.append('media', media);

    const {
      data,
    } = await api.post(RoutePrefix.MEME, body);

    dispatch({
      type: MemeActionsTypes.POST_MEME,
    });
    // eslint-disable-next-line no-console
    console.log(data);
  } catch (e) {
    dispatch({
      type: MemeActionsTypes.ERROR_POST_MEME,
    });
    // eslint-disable-next-line no-console
    console.log(e?.response);
  }
};
