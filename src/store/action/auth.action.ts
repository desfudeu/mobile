import auth from '@react-native-firebase/auth';

// import messaging from '@react-native-firebase/messaging';

import { GoogleSignin } from '@react-native-google-signin/google-signin';

import { Dispatch } from 'redux';

import {
  displayErrorMessage,
  displaySuccessMessage,
} from 'helpers/displayMessage';

import api from 'services/api';

import { StoreUserAction, AuthActionsTypes } from 'store/types/auth.types';

let authenticated = false;

export const configureAuthStateChangeCallback = (
  authenticatedCallback?: () => void,
  unauthenticatedCallback?: () => void,
) => (dispatch: Dispatch<StoreUserAction>): void => {
  auth().onAuthStateChanged(async (user) => {
    if (user) {
      if (!authenticated) {
        authenticated = true;
        const {
          displayName, email, photoURL, uid,
        } = user;
        dispatch({
          type: AuthActionsTypes.SIGNIN,
          payload: {
            user_name: `${displayName}`,
            user_email: `${email}`,
            user_id: uid,
            photo_url: `${photoURL}`,
          },
        });

        const token = await user.getIdToken();

        api.defaults.headers.common['access-token'] = token;

        authenticatedCallback?.();

        displaySuccessMessage('alerts.signin-success');
      }
    } else {
      authenticated = false;
      dispatch({ type: AuthActionsTypes.SIGNOUT });

      unauthenticatedCallback?.();
    }
  });
};

export const signinWithGoogle = () => async (
  dispatch: Dispatch<StoreUserAction>,
): Promise<void> => {
  dispatch({
    type: AuthActionsTypes.SIGNIN_ATTEMPT,
  });
  try {
    // Get the users ID token
    const { idToken } = await GoogleSignin.signIn();

    // Create a Google credential with the token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);

    // Sign-in the user with the credential
    await auth().signInWithCredential(googleCredential);
  } catch (e) {
    displayErrorMessage('alerts.google-signin-fail');
    dispatch({
      type: AuthActionsTypes.SIGNOUT,
    });
  }
};

export const signout = ():void => {
  displaySuccessMessage('alerts.signout-success');
  auth().signOut();
};
