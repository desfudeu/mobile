import api, { RoutePrefix } from 'services/api';
import { Dispatch } from 'redux';
import {
  ReactionPayloadTypes, ReactionActionTypes, ReactionType, Reaction, ReactionObjectMap,
} from 'store/types/reaction.types';

const generateReactionObjectMap = (reactions: Reaction[]) => {
  const reactionObjectMap: ReactionObjectMap = {};

  reactions.forEach((reaction) => {
    reactionObjectMap[`${reaction.memeId}`] = reaction.reactionType;
  });

  return reactionObjectMap;
};

export const getUserReactions = () => async (
  dispatch: Dispatch<ReactionPayloadTypes>,
):Promise<void> => {
  dispatch({
    type: ReactionActionTypes.LOADING_USER_REACTION,
  });

  try {
    const { data } = await api.get(RoutePrefix.REACTION);
    dispatch({
      type: ReactionActionTypes.USER_REACTION,
      payload: generateReactionObjectMap(data.reactions),
    });
  } catch (e) {
    dispatch({
      type: ReactionActionTypes.ERROR_USER_REACTION,
    });
  }
};

export const reactToMeme = (
  reactionType: ReactionType,
  memeId: string,
) => async (dispatch: Dispatch<ReactionPayloadTypes>):Promise<void> => {
  dispatch({
    type: ReactionActionTypes.LOADING_USER_REACTION,
  });
  try {
    const body = {
      reactionType,
      memeId,
    };

    const { data } = await api.post(RoutePrefix.REACTION, body);
    dispatch({
      type: ReactionActionTypes.USER_REACTION,
      payload: generateReactionObjectMap(data.reactions),
    });
  } catch (e) {
    dispatch({
      type: ReactionActionTypes.ERROR_USER_REACTION,
    });
  }
};

export const removeReactionFromMeme = (
  reactionType: ReactionType,
  memeId: string,
) => async (dispatch: Dispatch<ReactionPayloadTypes>):Promise<void> => {
  dispatch({
    type: ReactionActionTypes.LOADING_USER_REACTION,
  });
  try {
    const body = {
      reactionType,
      memeId,
    };

    const { data } = await api.post(`${RoutePrefix.REACTION}/remove`, body);

    dispatch({
      type: ReactionActionTypes.USER_REACTION,
      payload: generateReactionObjectMap(data.reactions),
    });
  } catch (e) {
    dispatch({
      type: ReactionActionTypes.ERROR_USER_REACTION,
    });
  }
};
