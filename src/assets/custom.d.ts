declare module '*.svg' {
  const content: string;
  export default content;
}

declare module '*.mp4' {
  const content: { uri?: string, headers?: { [key: string]: string } } | number;
  export default content;
}
