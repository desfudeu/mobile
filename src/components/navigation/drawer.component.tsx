import React, { useCallback } from 'react';
import {
  useDispatch,
  useSelector,
} from 'react-redux';
import {
  View,
  StyleSheet,
} from 'react-native';
import {
  Text,
  SVG,
  Button,
  Avatar,
} from 'components/common';
import {
  signinWithGoogle,
  signout,
} from 'store/action/auth.action';
import { createPost } from 'store/action/meme.action';
import {
  Colors,
  FontSizes,
  Spacing,
} from 'theme';
import { ApplicationState } from 'store';
import accessDeniedXml from 'assets/icon/access-denied.svg';

import DocumentPicker from 'react-native-document-picker';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.FULL_SHADE,
    alignItems: 'center',
    padding: Spacing.M,
  },
});

const ACCESS_DENIED_SIZE = 128;

export default (): React.ReactElement => {
  const dispatch = useDispatch();

  const {
    user,
    loading_auth,
  } = useSelector((state: ApplicationState) => state.authReducer);

  const onPressSigninWithGoogle = useCallback(() => {
    dispatch(signinWithGoogle());
  }, []);

  const onPressCreatePost = useCallback(async () => {
    try {
      const meme = await DocumentPicker.pick({
        type: [
          DocumentPicker.types.images,
          DocumentPicker.types.video,
        ],
      });

      dispatch(createPost(
        'blau',
        meme,
      ));
    } catch (e) {
      if (!DocumentPicker.isCancel(e)) {
        throw e;
      }
    }
  }, []);

  return (
    <View style={styles.container}>
      {
        !user?.user_id
          ? (
            <>
              <SVG
                xml={accessDeniedXml}
                width={ACCESS_DENIED_SIZE}
                height={ACCESS_DENIED_SIZE}
              />
              <Text
                value="drawer.unauthenticated-title"
                fontSize={FontSizes.H3}
                textAlign="center"
              />
              <Text
                value="drawer.unauthenticated-instructions"
                fontSize={FontSizes.H4}
                textAlign="center"
              />
              <Button
                fullWidth
                loading={loading_auth}
                onPress={onPressSigninWithGoogle}
                label="drawer.signin-with-google"
              />
            </>
          )
          : (
            <>
              <Avatar source={{
                uri: user.photo_url,
              }}
              />
              <Text
                value={`${user.user_name}`}
                fontSize={FontSizes.H3}
                textAlign="center"
              />
              <Button
                fullWidth
                onPress={onPressCreatePost}
                label="drawer.create-post"
              />
              <Button
                secondary
                fullWidth
                onPress={signout}
                label="drawer.signout"
              />
            </>
          )
      }
    </View>
  );
};
