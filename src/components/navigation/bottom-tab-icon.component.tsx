import React from 'react';
import {
  SVG,
} from 'components/common';

interface Props {
  icon: string
  size: number
  focused: boolean
}

export default ({
  icon,
  size,
  focused,
}: Props): React.ReactElement => (
  <SVG
    xml={icon}
    width={size}
    height={size}
    style={{
      opacity: focused ? 1 : 0.6,
    }}
  />
);
