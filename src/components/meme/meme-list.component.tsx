import React from 'react';
import {
  useSelector,
} from 'react-redux';
import {
  FlatList,
  StyleSheet,
} from 'react-native';
import {
  Meme,
} from 'store/types/meme.types';
import { Spacing } from 'theme';
import { ApplicationState } from 'store';
import MemeCard from './meme.card.component';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainer: {
    width: '100%',
    paddingTop: Spacing.L,
  },
});

interface Props {
  memes: Meme[]
  onRefresh: () => void
  refreshing: boolean,
  onEndReached: () => void
}

export default ({
  memes,
  onRefresh,
  refreshing,
  onEndReached,
}: Props): React.ReactElement => {
  const {
    user_reactions,
  } = useSelector((state: ApplicationState) => state.reactionReducer);

  return (
    <FlatList
      style={styles.container}
      contentContainerStyle={styles.contentContainer}
      data={memes}
      renderItem={({
        item,
      }) => <MemeCard meme={item} userReaction={user_reactions[item._id]} />}
      keyExtractor={(item) => `${item._id}`}
      onRefresh={onRefresh}
      refreshing={refreshing}
      onEndReached={onEndReached}
    />
  );
};
