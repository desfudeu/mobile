import React, {
  useState,
  useEffect,
  useCallback,
} from 'react';
import {
  useDispatch,
} from 'react-redux';
import {
  Dimensions,
  View,
  StyleSheet,
  Image as RNImage,
} from 'react-native';
import {
  Text,
  Image,
  Button,
} from 'components/common';
import {
  Meme,
} from 'store/types/meme.types';
import {
  FontSizes,
  Spacing,
} from 'theme';

import upvoteIcon from 'assets/icon/upvote.svg';
import downvoteIcon from 'assets/icon/downvote.svg';
import { reactToMeme, removeReactionFromMeme } from 'store/action/reaction.action';
import { ReactionType } from 'store/types/reaction.types';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  memeContainer: {
    width: '100%',
    marginBottom: Spacing.XL,
  },
  title: {
    margin: Spacing.XS,
  },
  meme: {
    width: '100%',
  },
  reactionContainer: {
    flexDirection: 'row',
    width: 110,
    justifyContent: 'space-between',
    marginLeft: Spacing.S,
  },
});

interface Props {
  meme: Meme;
  userReaction?: ReactionType
}

export default ({
  meme,
  userReaction,
}: Props): React.ReactElement => {
  const [memeHeight, setMemeHeight] = useState(0);

  const dispatch = useDispatch();

  useEffect(() => {
    if (meme.mediaUrl) {
      RNImage.getSize(meme.mediaUrl, (originalWidth, originalHeight) => {
        const height = Math.round((width / originalWidth) * originalHeight);
        setMemeHeight(height);
      });
    }
  }, [meme]);

  const onToggleUpvote = useCallback(() => {
    if (userReaction === ReactionType.UPVOTE) {
      dispatch(removeReactionFromMeme(
        ReactionType.UPVOTE,
        meme._id,
      ));
    } else {
      dispatch(reactToMeme(
        ReactionType.UPVOTE,
        meme._id,
      ));
    }
  }, []);

  const onToggleDownvote = () => {
    if (userReaction === ReactionType.DOWNVOTE) {
      dispatch(removeReactionFromMeme(
        ReactionType.DOWNVOTE,
        meme._id,
      ));
    } else {
      dispatch(reactToMeme(
        ReactionType.DOWNVOTE,
        meme._id,
      ));
    }
  };

  if (!memeHeight) return <></>;

  return (
    <View style={styles.memeContainer}>
      <Text
        style={styles.title}
        value={`${meme?.title?.toLowerCase?.() || ''}`}
        fontSize={FontSizes.H3}
      />
      <Image
        style={{
          ...styles.meme,
          height: memeHeight,
        }}
        source={{
          uri: meme.mediaUrl,
        }}
        resizeMode="cover"
      />
      <View
        style={styles.reactionContainer}
      >
        <Button
          icon={upvoteIcon}
          onPress={onToggleUpvote}
          secondary={userReaction !== ReactionType.UPVOTE}
        />
        <Button
          icon={downvoteIcon}
          onPress={onToggleDownvote}
          secondary={userReaction !== ReactionType.DOWNVOTE}
        />
      </View>
    </View>
  );
};
