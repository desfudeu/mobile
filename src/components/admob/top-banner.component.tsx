import React from 'react';
import {
  Platform,
  StyleSheet,
  View,
} from 'react-native';
import Config from 'react-native-config';
import {
  BannerAd,
  BannerAdSize,
  TestIds,
} from '@react-native-firebase/admob';
import { Colors } from 'theme';

const getBannerId = () => {
  if (__DEV__) {
    return TestIds.BANNER;
  }
  return Platform.select({
    ios: Config.IOS_TOP_BANNER_ID,
    android: Config.ANDROID_TOP_BANNER_ID,
  }) || TestIds.BANNER;
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.DARK_SHADE,
  },
});

export default (): React.ReactElement => (
  <View style={styles.container}>
    <BannerAd
      unitId={getBannerId()}
      size={BannerAdSize.ADAPTIVE_BANNER}
      onAdLoaded={() => {
        if (__DEV__) {
          // console.log('loaded')
        }
      }}
      onAdClosed={() => {
        if (__DEV__) {
          // console.log('closed')
        }
      }}
      onAdOpened={() => {
        if (__DEV__) {
          // console.log('opened')
        }
      }}
      onAdFailedToLoad={() => {
        if (__DEV__) {
          // console.log('onAdFailedToLoad', e)
        }
      }}
      onAdLeftApplication={() => {
        if (__DEV__) {
          // console.log('onAdLeftApplication')
        }
      }}
    />
  </View>
);
