import React from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import { Colors, FontFamilies, FontSizes } from 'theme';

import Text from './text.component';
import SVG from './svg.component';

const ICON_SIZE = 32;
const PADDING = 8;

const styles = StyleSheet.create({
  container: {
    height: ICON_SIZE + (PADDING * 2),
    backgroundColor: Colors.PRIMARY,
    borderWidth: 1.5,
    borderColor: 'transparent',
    padding: PADDING,
    marginVertical: 8,
    borderRadius: 8,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  secondaryContainer: {
    backgroundColor: 'transparent',
    borderColor: Colors.PRIMARY,
    alignSelf: 'flex-start',
  },
  fullWidthContainer: {
    width: '100%',
  },
});

interface Props {
  onPress: () => void;
  label?: string;
  icon?: string;
  loading?: boolean;
  secondary?: boolean;
  fullWidth?: boolean;
}

export default ({
  onPress,
  label,
  icon,
  loading,
  secondary,
  fullWidth,
}: Props): React.ReactElement => (
  <TouchableOpacity
    activeOpacity={0.65}
    onPress={onPress}
    style={[
      styles.container,
      secondary && styles.secondaryContainer,
      fullWidth && styles.fullWidthContainer,
    ]}
  >
    {
      loading
        ? (<ActivityIndicator size="large" color={Colors.EMPTY_SHADE} />)
        : (
          <>
            {
              icon
                ? (
                  <SVG
                    xml={icon}
                    fill={secondary ? Colors.PRIMARY : Colors.EMPTY_SHADE}
                    width={ICON_SIZE}
                    height={ICON_SIZE}
                  />
                ) : null
            }
            {
              label
                ? (
                  <Text
                    value={label}
                    fontSize={FontSizes.H5}
                    color={secondary ? Colors.PRIMARY : undefined}
                    fontFamily={FontFamilies.PRIMARY_MEDIUM}
                  />
                ) : null
            }
          </>
        )
    }
  </TouchableOpacity>
);
