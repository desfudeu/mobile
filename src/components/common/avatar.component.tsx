import React from 'react';
import {
  StyleSheet,
} from 'react-native';
import {
  Source,
} from 'react-native-fast-image';
import Image from './image.component';

const AVATAR_SIZE_LARGE = 64;

const styles = StyleSheet.create({
  container: {
    width: AVATAR_SIZE_LARGE,
    height: AVATAR_SIZE_LARGE,
    borderRadius: AVATAR_SIZE_LARGE / 2,
  },
});

interface Props {
  source: Source,
}

export default ({
  source,
}: Props): React.ReactElement => (
  <Image
    source={source}
    style={styles.container}
  />
);
