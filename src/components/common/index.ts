export { default as Button } from './button.component';
export { default as SVG } from './svg.component';
export { default as Text } from './text.component';
export { default as Image } from './image.component';
export { default as Avatar } from './avatar.component';
