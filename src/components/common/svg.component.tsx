import React from 'react';
import { SvgXml, XmlProps } from 'react-native-svg';

export default (props: XmlProps): React.ReactElement => (
  // eslint-disable-next-line react/jsx-props-no-spreading
  <SvgXml {...props} />
);
