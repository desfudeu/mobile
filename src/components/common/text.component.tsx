import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Text,
  StyleSheet,
  TextStyle,
} from 'react-native';

import {
  FontFamilies,
  FontSizes,
  Colors,
} from 'theme';

interface Props {
  value: string;
  style?: TextStyle;
  fontFamily?: FontFamilies;
  fontSize?: FontSizes;
  color?: Colors | string;
  textAlign?: 'auto' | 'left' | 'right' | 'center' | 'justify';
}

export default ({
  value,
  style = {},
  fontFamily = FontFamilies.PRIMARY_REGULAR,
  fontSize = FontSizes.H6,
  color = Colors.EMPTY_SHADE,
  textAlign = 'left',
}: Props): React.ReactElement => {
  const { t: translate } = useTranslation();

  const styles = useMemo(() => StyleSheet.create({
    text: {
      fontFamily,
      fontSize,
      color,
      textAlign,
      ...style,
    },
  }), [
    fontFamily,
    fontSize,
    color,
    textAlign,
    style,
  ]);

  return (
    <Text
      style={styles.text}
    >
      {translate(value)}
    </Text>
  );
};
