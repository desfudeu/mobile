import React, {
  useState,
} from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  View,
} from 'react-native';
import FastImage, {
  ResizeMode,
  Source,
  ImageStyle,
} from 'react-native-fast-image';

const styles = StyleSheet.create({
  loadContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  load: {
    position: 'absolute',
  },
});

interface ImageProps {
  style: ImageStyle;
  source: Source;
  resizeMode?: ResizeMode;
  tintColor?: string;
  loading?: boolean;
  children?: React.ReactChildren
}

export default ({
  style,
  source,
  resizeMode,
  tintColor,
  children,
  loading,
}: ImageProps): React.ReactElement => {
  const [onLoad, setLoading] = useState(true);

  return (
    <FastImage
      style={[
        style,
        loading && onLoad && styles.loadContainer,
      ]}
      source={source}
      resizeMode={resizeMode || 'contain'}
      tintColor={tintColor}
      onLoadEnd={() => {
        setLoading(false);
      }}
    >
      {loading && onLoad && (
        <View style={styles.load}>
          <ActivityIndicator />
        </View>
      )}
      {onLoad ? null : children}
    </FastImage>
  );
};
